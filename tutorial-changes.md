## General notes

The [tutorial](https://www.sitepoint.com/angular-2-tutorial/) is a little outdated so there are a few changes listed below.  If you're copying/pasting directly from the tutorial, Typescript will give warnings around the usage of `let` instead of `const` and type annotation when it can be inferred from the value being assigned.  You will see recommended fixes (in VSCode) for these. 

1:  Install Angular CLI
```
npm install -g @angular/cli
```
2: Install todo-mvc css
```
npm install --save npm install todomvc-app-css
```
3: Add stylesheet to `angular.json` file in `architect/build` options
```
          ...,
            "styles": [
              "src/styles.scss",
              "./node_modules/todomvc-app-css/index.css"
            ],
          ...,
```

2: Generate `Todo` class
```
ng generate class models/todo
```  
3: Add logic to Todo class
```
export class Todo {
  id: number;
  title: string;
  complete: boolean;

  constructor(values: object = {}) {
    Object.assign(this, values);
  }
}
```
4: Add additional test
```
  it('should accept values in the constructor', () => {
    const todo = new Todo({
      title: 'hello',
      complete: true
    });
    expect(todo.title).toEqual('hello');
    expect(todo.complete).toEqual(true);
  });
```

5: Create TodoDataService
```
ng generate service services/TodoData
```

6: When updating `AppComponent` DON'T add `TodoDataService` as a provider as it has already been registered globally

7: Import forms module into app.module.ts. `import { FormsModule } from '@angular/forms';` and add to module imports
```

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```
8: In `app.component.spec.ts` don't wrap any tests in `async()`

9: In `app.component.spect.ts`  delete `fixture.detectChanges()` in the `should display "Todos" in h1 tag` test
