# Ng-Todo

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.8.

It is based on the Todo app created with [this](https://www.sitepoint.com/angular-2-tutorial/) tutorial.  The tutorial uses an older version of Angular & the CLI.  If you're following the tutorial from the beginning there are some guidance [here](./tutorial-changes.md) of some syntatic changes.

NOTE: The tutorial uses Karma & Jasmine, not Jest, but the tests are essentially the same

The `master` branch is a vanilla Angular app with Jest set up for running tests.

The `part-1` branch is the tutorial app ready for unit tests.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `npm test` to execute the unit tests via [Jest](https://jestjs.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
